angular.module('couponFilters', []).filter('notExpired', function() {
	return function(coupons) {
		//console.log("coupons: ",coupons);
		var notExpiredCoupons = [];
		var now = new Date();
		var expireDate;

		if(coupons !== undefined) {
			for(var i =0; i< coupons.length; i++) {
				expireDate = new Date(coupons[i].expireDatetime["iso"]);
				//console.log('expireDate ',expireDate);
				if(now < expireDate) {
					//console.log("now < expireDate");
					notExpiredCoupons.push(coupons[i]);
				}
			}
		}
		return notExpiredCoupons;
	};
});