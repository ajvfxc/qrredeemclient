// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js

(function() {

  angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'couponFilters', 'ngMessages']) //register couponFilters custom filter

  .run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleLightContent();
      }
    });
    //check if logged in by checking localstorage if sessionToken exists

   // $rootScope.loggedIn = 
  })

  .config(function($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

    // setup an abstract state for the tabs directive
      .state('tab', {
      url: '/tab',
      abstract: true,
      templateUrl: 'templates/tabs.html'
    })

    // Each tab has its own nav history stack:

    .state('tab.dash', {
      url: '/dash',
      views: {
        'tab-dash': {
          templateUrl: 'templates/tab-dash.html',
          controller: 'DashCtrl'
        }
      }
    })

    .state('tab.chats', {
        url: '/chats',
        views: {
  // searches for a <ion-nav-view name="tab-chats"></ion-nav-view> directive tag then takes the 'templates/tab-chats.html' and puts it between them
          'tab-chats': {
            templateUrl: 'templates/tab-chats.html',
            controller: 'ChatsCtrl'
          }
        }
      })
      .state('tab.chat-detail', {
        url: '/chats/:chatId', //full url: /tab/chats/:chatId  (/tab comes from the tab state this state is under)
        views: {
          'tab-chats': {
            templateUrl: 'templates/chat-detail.html',
            controller: 'ChatDetailCtrl'
          }
        }
      })

    //saved coupons
    .state('tab.account', {
      url: '/account',
      views: {
        'tab-account': {
          templateUrl: 'templates/tab-account.html',
          controller: 'AccountCtrl'
        }
      }
    })
    //details from saved coupons
    .state('tab.account-detail', {
      url: '/account/:chatId', //full url: /tab/chats/:chatId  (/tab comes from the tab state this state is under)
      views: {
        'tab-account': { // the view name should match the above so the it's the same navigation stack and 
        //the back button works (separately for each tab)
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })
    .state('tab.qr', {
      url: '/show-qr',
      views: {
        'tab-chats': {
          templateUrl: 'templates/show-qr.html',
          controller: 'ShowQrCtrl'
        }

      }
    })

    .state('login', {
      url: '/login?redirect', //for specifying the redirect url for $stateParams service
      views: {
        '' : { //searches for a <ion-nav-view></ion-nav-view> without a name ="..." attribute, and puts 'templates/login.html' there
              // there can only be 1 <ion-nav-view> without a name in the application
          templateUrl: 'templates/login.html',
          controller: 'LoginCtrl'
        }
      }
    })

    .state('register', {
      url: '/register',
      views: {
        '' : { //searches for a <ion-nav-view></ion-nav-view> without a name ="..." attribute, and puts 'templates/login.html' there
              // there can only be 1 <ion-nav-view> without a name in the application
          templateUrl: 'templates/register.html',
          controller: 'RegisterCtrl'
        }
      }
    })
  ;
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/tab/chats');

  });
})();
