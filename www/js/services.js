angular.module('starter.services', [])

.factory('Chats', ['$http', '$q',function($http,$q) {
  // Might use a resource here that returns a JSON array

  var chats;
  
  // Some fake testing data
// var chats = [{"amount":5,"createdAt":"2015-07-27T03:58:06.517Z","description":"free chicken",
// "expireDatetime":{"__type":"Date","iso":"2015-07-28T05:59:00.000Z"},"objectId":"sFqF7sNb3i","title":"KFCChicken",
// "totalAmount":10,"updatedAt":"2015-07-27T05:59:08.259Z"},{"amount":10,"createdAt":"2015-07-27T03:58:47.146Z",
// "description":"free nugggets","expireDatetime":{"__type":"Date","iso":"2015-07-29T05:59:00.000Z"},
// "objectId":"EfpegE7Ihn","title":"McDNuggets","totalAmount":10,"updatedAt":"2015-07-27T05:59:34.321Z"}];

  return {
    all: function () {
      var deferred = $q.defer();
      if(chats !== undefined)
        deferred.resolve(chats);
      else{
        $http.get('https://api.parse.com/1/classes/Coupon', {
              headers: {
                "X-Parse-Application-Id": "PHMSnxrbPHQLgOBuE6XbXcjFosZ5QW791H1SLP7p",
                "X-Parse-REST-API-Key": "uClAe1OJ7miqLjxTtGROUwqymJ94arQM6Ly55s3u"
            }
          })
        .success(function(data) {
            //console.log('Parse data',data);
            chats = data.results;
            deferred.resolve(chats);
          })
          .error(function(data){
            console.log("error!", data);
            deferred.reject(data);
          });
      }
      return deferred.promise;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    //this way when we change the coupon amount in the coupon details page, the corresponding amount
    // in the home page automatically gets updated
    get: function(objectId) {
      var deferred = $q.defer();
      //console.log('objectId = ',objectId);
      if(chats !== undefined) {
        for (var i = 0; i < chats.length; i++) {
          //console.log('chats =',chats[i].objectId);

          if (chats[i].objectId === objectId) {
            deferred.resolve(chats[i]);
          }
        }
    }
    else{
    console.log('not found');
      // not found, when page is accessed directly not through index
      // shouldn't get in here as chats obj already loaded

      chats = [];

      $http.get('https://api.parse.com/1/classes/Coupon/' + objectId, {
              headers: {
                "X-Parse-Application-Id": "PHMSnxrbPHQLgOBuE6XbXcjFosZ5QW791H1SLP7p",
                "X-Parse-REST-API-Key": "uClAe1OJ7miqLjxTtGROUwqymJ94arQM6Ly55s3u"
            }
          })
        .success(function(data) {
            //console.log('Parse data',data);
            chats.push(data); //push into chats obj
            deferred.resolve(data);
          })
          .error(function(data){
            console.log("error!", data);
            deferred.reject(data);
          });
  }
      return deferred.promise;
    }
  };
}])

//service for accessing localstorage
.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    },
    clear: function() {
      $window.localStorage.clear();
    },
    removeItem: function(key) {
      $window.localStorage.removeItem(key);
    }
  };
}])

//service for checking if user is logged in
.factory('$login', ['$localstorage', '$http', '$ionicPopup', '$location', function($localstorage, $http, $ionicPopup, $location) {
  return {
    isLoggedin: function() {
      if($localstorage.get('sessionToken') === undefined)
        return false;
      else
        return true;
    },
    //log in to Parse using its REST API and return a promise for the caller to handle
    loginParse: function(username, password) { //the function login is reserved in ionic? Doesn't work.
      var loginUrl = 'https://api.parse.com/1/login';
      var config = {
        params: {
          'username' : username,
          'password' : password
        },
        headers: {
          "X-Parse-Application-Id": "PHMSnxrbPHQLgOBuE6XbXcjFosZ5QW791H1SLP7p",
          "X-Parse-REST-API-Key": "uClAe1OJ7miqLjxTtGROUwqymJ94arQM6Ly55s3u"
        }
      };
      return $http.get(loginUrl, config);
    },
    loginVerify: function(cancelUrl) { //if logged in, returns true, else returns false and pops up login dialog box
      //show dialog box with login page link
      if(!this.isLoggedin()) {
        var loginPopup = $ionicPopup.confirm({
          title: 'Login',
          template: 'Please log in to use this feature.'
        });
        loginPopup.then(function(response) {
          if(response) {
            var prevUrl = $location.url();
            console.log(prevUrl);
            $location.url('/login?redirect=' + prevUrl); //specify redirect url after login
          }
          else { //USER PRESSED CANCEL
            if(cancelUrl !== undefined)
              $location.url(cancelUrl);
          }
        },
        function(response) {
          console.log("loginpopup error");
        });
        return false;
      }
      else {
        return true;
      }
    }
  };
}])
// service for getting the coupons that we've already requested. (pressed rubcoupon)
.factory('$couponReq', ['$http', '$localstorage', '$stateParams', function($http, $localstorage, $stateParams) {
  var savedNeedsUpdate = true; //bool var to check if the info in the saved tab needs to be updated e.g. when a new coupon request gets created
  return {
    get : function() {
      var userId = $localstorage.get('objectId');
      var sessionToken = $localstorage.get('sessionToken');
      var config = {
        'headers' : {
          "X-Parse-Application-Id": "PHMSnxrbPHQLgOBuE6XbXcjFosZ5QW791H1SLP7p",
          "X-Parse-REST-API-Key": "uClAe1OJ7miqLjxTtGROUwqymJ94arQM6Ly55s3u",
          'X-Parse-Session-Token' : sessionToken
        },
        'params' : {
          'where' : {
            'userId' : userId,
            'couponId' : $stateParams.chatId
            //'redeemed' : {"$ne" : false}
          }
        }
      };
      return $http.get('https://api.parse.com/1/classes/CouponRequest', config);
    },
    getSavedNeedsUpdate: function() {return savedNeedsUpdate;},
    setSavedNeedsUpdate: function(val) {savedNeedsUpdate = val;}
  };

}]);
