angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) { //2nd arg is to inject the Chats service dependency
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});


  // Promise method 1
  /*Chats.all().success(function(data) {
    $scope.chats = data.results;
    console.log('Parse data',data.chats);
  })
  .error(function(data){
    console.log("error!", data);
  });*/

  // Promise method 2
  /*Chats.all().then(function(response) {
    console.log('Parse data',response);
    $scope.chats = response.data.results;
  },function(error){
    console.log("error!", error);
  });*/

  Chats.all().then(function(chats) {
    $scope.chats = chats;

  },function(error){
    console.log("error!", error);
  });

  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', ['$scope', '$stateParams', 'Chats', '$login', '$ionicPopup', '$location',
  '$http', '$localstorage', '$ionicModal', '$couponReq',
  function($scope, $stateParams, Chats, $login, $ionicPopup, $location, $http, $localstorage, $ionicModal, $couponReq) {
  //console.log($stateParams.chatId);
  $scope.qrCode = '';
  $scope.disableGet = false;
  $scope.disableUse = false;

  Chats.get($stateParams.chatId).then(
    function(data){
      var now = new Date();
      var expireDate = new Date(data.expireDatetime["iso"]);
      //console.log('data: ', data);
      //put expired boolean
      if(now >= expireDate)
        data['expired'] = true;
      else
        data['expired'] = false;

      $scope.chat = data;
      console.log(data);
      },
    function() {
      console.log("ChatDetailCtrl(): promise error");
    });

  $scope.alert = function() {
    alert("coupon redeemed");
  };

  $scope.getCoupon = function() {
    if($login.loginVerify()) {
      console.log("getCoupon(): logged in");
      //POST to parse.com to create a CouponRequest
      var userId = $localstorage.get('objectId');
      //console.log("getcoupon(): userId: ", userId);
      var sessionToken = $localstorage.get('sessionToken');
      var config = {
        'headers' : {
          "Content-Type": "application/json",
          "X-Parse-Application-Id": "PHMSnxrbPHQLgOBuE6XbXcjFosZ5QW791H1SLP7p",
          "X-Parse-REST-API-Key": "uClAe1OJ7miqLjxTtGROUwqymJ94arQM6Ly55s3u",
          'X-Parse-Session-Token' : sessionToken
        }
      };
      var bodyData = {
        'couponId' : $stateParams.chatId,
        'userId' : userId,
        'ACL' : {
         // userId : {'read' : true, 'write' : false} //won't work as 'userId' is literally treated as an obj property
        }
      };
      bodyData["ACL"][userId] = {'read' : true, 'write' : true};
      //console.log("bodyData: ",bodyData);

      $http.post('https://api.parse.com/1/classes/CouponRequest', bodyData, config)
      .success(function(data) {
        console.log("parse.com returned POST CouponRequest success: ", data);
        //tell that the saved tab content needs updating
        $couponReq.setSavedNeedsUpdate(true);
        //success msg
        (function() {
          var alertPopup = $ionicPopup.alert({
            title: 'Success',
            template: 'คุณได้รับคูปองเรียบร้อยแล้ว'
          });
          alertPopup.then(function(res) {
            $scope.disableGet = false; //reenable button
          });
        })();
      })
      .error(function(data) {
        console.log("parse.com returned POST CouponRequest error: ", data);

        (function() {
          var alertPopup = $ionicPopup.alert({
            title: 'Error Message',
            template: 'ไม่สามารถรับคูปองได้ กรุณาลองใหม่อีกครั้ง'
          });
          alertPopup.then(function(res) {
            $scope.disableGet = false; //reenable button
          });
        })();
      });
    }
  };

  //use custom getCoupon function defined in cloud
  $scope.customGetCoupon = function() {
    var userId, sessionToken, config, bodyData;

    $scope.disableGet = true;
    if($login.loginVerify()) {
      userId = $localstorage.get('objectId');
      sessionToken = $localstorage.get('sessionToken');
      config = {
        'headers' : {
          "Content-Type": "application/json",
          "X-Parse-Application-Id": "PHMSnxrbPHQLgOBuE6XbXcjFosZ5QW791H1SLP7p",
          "X-Parse-REST-API-Key": "uClAe1OJ7miqLjxTtGROUwqymJ94arQM6Ly55s3u",
          'X-Parse-Session-Token' : sessionToken
        }
      };
      bodyData = {
        'couponId' : $stateParams.chatId, //get couponId from URL
        'userId' : userId,
        'redeemed' : false
      };
      $http.post('https://api.parse.com/1/functions/newCouponRequest', bodyData, config)
      .then(function(response) {
        console.log('custom coupon request response: ', response);
        $couponReq.setSavedNeedsUpdate(true);
        // decrement remaining coupon count on this view
        $scope.chat.amount = response.data.result.remaining_amount;
        //success msg
        (function() {
          var alertPopup = $ionicPopup.alert({
            title: 'Success',
            template: 'คุณได้รับคูปองเรียบร้อยแล้ว'
          });
          alertPopup.then(function(res) {
            console.log("successfully got coupon", res);
            $scope.disableGet = false; //reenable button
          });
        })();
      },
      function(err) {
        console.log('error from custom coupon request: ', err);
        if(err.data.error.localeCompare("No more coupons left") === 0) {
          $ionicPopup.alert({
            title : 'Error',
            template : 'คูปองชนิดนี้หมดแล้ว ไม่สามารถรับเพิ่มได้'
          }).then(function(res) {
            console.log("0 coupons remaining alert", res);
            $scope.disableGet = false; //reenable button
          });
        }
        else {
          (function() {
            var alertPopup = $ionicPopup.alert({
              title: 'Error Message',
              template: 'ไม่สามารถรับคูปองได้ กรุณาลองใหม่อีกครั้ง'
            });
            alertPopup.then(function(res) {
              $scope.disableGet = false; //reenable button
            });
          })();
        }
      });
    } else $scope.disableGet = false;
  };



  $scope.openQrModal = function() {
    $scope.disableUse = true;
    //login check
    if($login.loginVerify()) {
      $couponReq.get().then(function(data) { // get all the couponReqs from parse server that match our couponID given through $stateParams
      //$scope.disableUse = false;
                
        var unredeemedRequests;
        console.log('coupon requests: ', data);
        //couponRequests = data;
        //if there are no couponRequests, tell the user to press the 'rubCoupon' button
        if(data.data.results.length > 0) {
          //filter out the redeemed coupons
          unredeemedRequests = data.data.results.filter(function(request) {
            return request.redeemed !== true;
          });

          if(unredeemedRequests.length > 0) {
            //When user clicks on 'Use Coupon' pop up a modal showing qr code
            $ionicModal.fromTemplateUrl('templates/show-qr.html', {
              scope: $scope,
              animation: 'slide-in-up'
            }).then(function(modal) {
              $scope.modal = modal;
              $scope.qrCode = unredeemedRequests[0].objectId;
              // pop up modal and draw QR code from objectId
              $scope.modal.show().then(function(mod) {
                console.log("In $scope.modal.show().then(function(mod) before new QRCode()");
                $scope.genQr = new QRCode("qrcode");
                $scope.genQr.clear();
                $scope.genQr.makeCode($scope.qrCode);
                $scope.disableUse = false;
              },
              function(mod) {
                console.log('error showing modal');
                $scope.disableUse = false;
              });
            },
            function(modal) {
                console.log("modal error");
                $scope.disableUse = false;
            });

          }
          else { // no matching coupon requests that are still unredeemed
            (function() {
              var alertPopup = $ionicPopup.alert({
                title: 'Error Message',
                template: 'คูปองชนิดนี้ได้ถูกredeemหมดแล้ว กรุณากรับคูปองเพิ่ม'
              });
              alertPopup.then(function(res) {
                $scope.disableUse = false;
              });
            })();
          }
        }
        else { //no coupon requests have been made yet
          //popup reminding user
          // An alert dialog
          console.log(" else: no coupon requests yet");
           $scope.showAlert = function() {
            var alertPopup = $ionicPopup.alert({
               title: 'ท่านยังไม่ได้รับคูปองนี้',
               template: 'โปรดกดรับคูปองก่อน'
             });
             alertPopup.then(function(res) {
               console.log('alert fired successfully');
               $scope.disableUse = false;
             });
           };
           $scope.showAlert();
        }
      },
      function(error) {
        $scope.disableUse = false;
        console.log('get coupon requests error: ', error);
      });
    } else $scope.disableUse = false;
  };

  $scope.closeModal = function() {
    console.log("closeModal()");
    document.getElementById("qrcode").innerHTML = "";
    $scope.modal.remove();
    $scope.disableUse = false;
  };

  $scope.$on('$destroy', function() {
    if($scope.modal !== undefined)
      $scope.modal.remove();
    console.log("modal removed");
  });

}])

.controller('ShowQrCtrl', function($scope) {

})

.controller('LoginCtrl', function($scope, $location, $http, $localstorage, $rootScope, $login, $stateParams) {
  $scope.user = {
    email:''
  }; // need this or else it screws up

  $scope.gotoRegister = function() {
    $location.url('/register');
  };

  // logs in by using $login service
  // foo = logmeIn();
  $scope.logMeIn = function() { //the function login is reserved in ionic? Doesn't work.
    $scope.loginFailure = false;
     var results = {'success': false, 'errormsg' : ''};
     var redirectUrl = $stateParams.redirect;

    //needs to be a $scope variable so ng-model can access it in the template
    $login.loginParse($scope.user.email, $scope.user.password).success(function(data) {
      console.log('Login success. Parse returned: ', data);
      results['success'] = true;
      console.log("success(): results obj declared in logMeIn():", results);

      //put sessionToken and other info in localstorage
      $localstorage.set('sessionToken', data.sessionToken);
      $localstorage.set('objectId', data.objectId);
      $localstorage.set('username', $scope.user.email);

      //go to home page after login
      console.log('$stateParams: ', $stateParams);
      if(redirectUrl !== undefined)
        $location.url($stateParams.redirect);
      else
        $location.url('/');

    }).error(function(data){
      $scope.loginFailure = true;
      $scope.errormsg = data.error;
      console.log('login error! ', data.error);
    });
    // cannot use the change of state resulting from the call backs in success/error below, as the code below will probably execute first
    //$scope.loginFailure = loginResults['success'];
    //console.log("results:", results);


    // var loginUrl = 'https://api.parse.com/1/login';
    // var config = {
    //   params: {
    //     'username' : $scope.user.email,
    //     'password' : $scope.user.password
    //   },
    //   headers: {
    //     "X-Parse-Application-Id": "PHMSnxrbPHQLgOBuE6XbXcjFosZ5QW791H1SLP7p",
    //     "X-Parse-REST-API-Key": "uClAe1OJ7miqLjxTtGROUwqymJ94arQM6Ly55s3u"
    //   }
    // };

    // $http.get(loginUrl, config).success(function(data) {
    //   console.log('Login success. Parse returned: ', data);
    // })
    // .error(function(data){
    //   $scope.errormsg = data.error;
    //   $scope.loginFailure = true;
    //   console.log('login error! ', data.error);
    // });

  };
})

.controller('RegisterCtrl', function($scope, $location, $http, $localstorage) {
  $scope.regSuccess = false;
  $scope.regFailure = false;
  $scope.user = {}; //TODO: see why you need a to declare a user object first

  $scope.sendToParse = function() {
    // console.log("email: ", $scope.user.email);
    // console.log("pass: ", $scope.user.password);
    // console.log("confirm: ", $scope.user.passConfirm);

    var sendData = {'username' : $scope.user.email,
                    'password' : $scope.user.passConfirm,
                    'email'    : $scope.user.email
    };

    $http.post('https://api.parse.com/1/users', sendData, {
      headers: {
        "X-Parse-Application-Id": "PHMSnxrbPHQLgOBuE6XbXcjFosZ5QW791H1SLP7p",
        "X-Parse-REST-API-Key": "uClAe1OJ7miqLjxTtGROUwqymJ94arQM6Ly55s3u"
      }
    }).success(function(data) {
      console.log('success! parse returned:', data);
      $scope.regSuccess =true;
      $scope.regFailure = false;
      //put sessionToken and other info in localstorage
      $localstorage.set('sessionToken', data.sessionToken);
      $localstorage.set('objectId', data.objectId);
      $localstorage.set('username', $scope.user.email);

      //clear form
      $scope.user.email = '';
      $scope.user.passConfirm = '';
      $scope.user.password = '';
      //reset form state
      //$scope.registration.$setPristine(); doesn't work for some reason
      //temp hack fix
      $scope.regSuccess = false;
      $location.url('/');
    })
    .error(function(data){
      console.log('parse returned registraton error', data);
      $scope.regFailure = true;
    });

  };
  
})
.controller('SideMenuCtrl', function($scope, $location, $http, $login, $localstorage) {
  console.log('SideMenuCtrl');

  $scope.loginStatus = $login.isLoggedin; //to show and hide email in side menu, as there ng-show=loginStatus()
  $scope.email = function() {return $localstorage.get('username');};

  $scope.displayLoginText = function() {
    if($login.isLoggedin())
      return 'Log out';
    else
      return 'Log in';
  };

  $scope.logOut =  function() {
    //deal with parse server
    var logoutUrl = 'https://api.parse.com/1/logout';
    var sessionToken = $localstorage.get('sessionToken');
    var config = {
      headers: {
        "X-Parse-Application-Id": "PHMSnxrbPHQLgOBuE6XbXcjFosZ5QW791H1SLP7p",
        "X-Parse-REST-API-Key": "uClAe1OJ7miqLjxTtGROUwqymJ94arQM6Ly55s3u",
        "X-Parse-Session-Token": sessionToken
      }
    };
    $http.post(logoutUrl, null, config).success(function(data) {
      console.log('Logout success. Parse returned: ', data);
      $localstorage.clear(); //delete sessionToken, etc
      //go to home page after logout
      $location.url('/');
    })
    .error(function(data) {
      $scope.errormsg = data.error;
      console.log('logout error! ', data.error, 'data: ', data);
    });
  };

  //function to run when user clicks on login/logout button
  $scope.loginLogout = function() {
    if($login.isLoggedin())
      $scope.logOut();
    else //go to login page
      $location.url('/login');
  };
})

//controller for the 'saved' tab
.controller('AccountCtrl', ['$scope', '$couponReq', 'Chats', '$login', '$ionicPopup', '$location', function($scope, $couponReq, Chats, $login, $ionicPopup, $location) {
  $scope.cr = {};
  $scope.unredeemedNum = function(requests) {
    var unredeemedReq = requests.filter(function(request) {
      return request.redeemed !== true;
    });
    return unredeemedReq.length;
  };
  //upon viewing the page, check if the contents (coupon requests) need updating
  $scope.$on('$ionicView.enter', function() {
    if($login.loginVerify('/')) {
      if($couponReq.getSavedNeedsUpdate()) {
        $couponReq.get().then(function(cRequests) { //get every coupon request, as when on this page, $stateParams.chatId is undefined. See $couponReq
          //console.log('coupon requests:', cRequests); //why is this the same as when logging the appended requests?
          
          //get coupons and descriptions and expireDatetime for matching requests
          //getting all the coupons and doing this n^2 2 array search is probably faster than making
          //separate http requests for the corrsesponding coupon object for every coupon request
          Chats.all().then(function(coupons) {
            var now = new Date();
            var crById = {};
            //console.log("coupons: ", coupons);
            cRequests.data.results.forEach(function(request) {
              var expireDate;
              //every cr should match with a coupon
              for(var i = 0, len = coupons.length; i<len; i++) {
                if(request.couponId === coupons[i].objectId) { //match
                  request['title'] = coupons[i]['title'];
                  request['expireDatetime'] = coupons[i]['expireDatetime'];

                  expireDate = new Date(request.expireDatetime["iso"]);
                  //add boolean var expired to each request
                  if(now >= expireDate)
                    request['expired'] = true;
                  else
                    request['expired'] = false;

                  //add to hash
                  if(request.couponId in crById) {
                    crById[request.couponId].push(request);
                  }
                  else {//couponId NOT in hash
                    crById[request.couponId] = [request];
                  }
                }
              }
            });
            console.log("appended cRequests: ", cRequests);
            console.log("crById: ", crById);
            $scope.cr.requests = cRequests;
            $scope.cr.crById = crById;
            // set no need to update lists
            $couponReq.setSavedNeedsUpdate(false);

          },function(error){
            console.log("get couponRequests error!", error);
          });

        }, function(error) {
          console.log("get coupon requests error");
        });
      }
    }
    // else { //NOT LOGGED IN
    //   //show dialog box with login page link
    //   var loginPopup = $ionicPopup.confirm({
    //     title: 'Login',
    //     template: 'Please log in to use this feature.'
    //   });
    //   loginPopup.then(function(response) {
    //     if(response) {
    //       var prevUrl = $location.url();
    //       console.log(prevUrl);
    //       $location.url('/login?redirect=' + prevUrl); //specify redirect url after login
    //     }
    //     else {
    //       $location.url('/');
    //     }
    //   },
    //   function(response) {
    //     console.log("loginpopup error");
    //   });
    // }
  });
}]);
